$(document).ready(function(){
    //url to api developed in django
    var baseUrl = 'http://fierce-dawn-3249.herokuapp.com/';
    // var baseUrl = "http://localhost:8000/";
  		var margin = {top: 20, right: 20, bottom: 30, left: 40},
                        width = 660 - margin.left - margin.right,
                        height = 300 - margin.top - margin.bottom;

        var x = d3.scale
                .ordinal()
                .rangeRoundBands([0, width], .1);

        var y = d3.scale
                .linear()
                .range([height, 0]);

        var xAxis = d3.svg
                    .axis()
                    .scale(x)
                    .orient("bottom");

        var yAxis = d3.svg
                    .axis()
                    .scale(y)
                    .orient("left")
                    .ticks(10, "");

        var svg = d3.select("#line-graph")
                    .append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        ages = [];
        scores = [];
        genders = [];
        boards = [];
        branches = [];
        cities = [];
        states = [];
        streams = [];
        //ajax call to student api to populate charts
        url = baseUrl+"api/student/";
        $.ajax({
            url: url,
            dataType: "jsonp",
            processData:  false,
            contentType: "application/json",
            crossDomain: true,
            success:function(data){
                $('#total-admissions').find('h2').html(data.meta.total_count);
                minAge = 1000;
                maxAge = 0;
                //calling function to prepare data as required for graphs
                prepareDataSet(data);
                //create bar chart for count vs date view
                create();
            }
        });
        //prepare filters based on the data received from api json
        function prepareFilters(value){
            if($.inArray(value.stream,streams) === -1){
                    $('#streams').append($('<option>', {
                        value: value.stream,
                        text : value.stream
                    }));
                    streams.push(value.stream);
            }
            if($.inArray(value.state,states) === -1){
                $('#states').append($('<option>', {
                    value: value.state,
                    text : value.state
                }));
                states.push(value.state);
            }
            if($.inArray(value.city,cities) === -1){
                $('#cities').append($('<option>', {
                    value: value.city,
                    text : value.city
                }));
                cities.push(value.city);
            }
            if($.inArray(value.branch,branches) === -1){
                $('#branches').append($('<option>', {
                    value: value.branch,
                    text : value.branch
                }));
                branches.push(value.branch);
            }
            if($.inArray(value.board,boards) === -1){
                $('#boards').append($('<option>', {
                    value: value.board,
                    text : value.board
                }));
                boards.push(value.board);
            }
            if($.inArray(value.gender,genders) === -1){
                $('#genders').append($('<option>', {
                    value: value.gender,
                    text : value.gender
                }));
                genders.push(value.gender);
            }
        }
        function prepareDataSet(data){
            temp = {};
            dataSet = [];
            //getting min,max age and score to prepare range based filters
            minAge = 1000;
            maxAge = 0;
            minScore = 1000;
            maxScore = 0;
            $.each(data.objects,function(key,value){
                if(value.age < minAge)
                    minAge = value.age;
                if(value.age > maxAge)
                    maxAge = value.age;
                if(value.score < minScore)
                    minScore = value.score;
                if(value.score > maxScore)
                    maxScore = value.score;
                prepareFilters(value);    
                dateString = value.created_at;
                dateStringArr = dateString.split('T');
                date = dateStringArr[0]
                if(temp[date])
                    temp[date] =  temp[date] + 1;
                else
                    temp[date] =  1;
            });
            keys = [];
            $.each(temp,function(key,value){
                keys.push(key);
            });
            keys.sort();
            $.each(keys,function(key,value){
                dataSet.push({date:value,count:temp[value]});
            });
            lowerFactor = parseInt(minAge/5);
            upperFactor = parseInt(maxAge/5);
            for(i=lowerFactor-1;i<=upperFactor;i++){
                t = ((i*5)+1)+"-"+(i+1)*5;
                $('#ages').append($('<option>', {
                    value: t,
                    text : t
                }));
            }
            lowerFactor = parseInt(minScore/5);
            upperFactor = parseInt(maxScore/5);
            for(i=lowerFactor-1;i<=upperFactor;i++){
                t = ((i*5)+1)+"-"+(i+1)*5;
                $('#scores').append($('<option>', {
                    value: t,
                    text : t
                }));
            }
        }
        //create bar chart on svg from the data prepared
        function create(){
            x.domain(
                dataSet.map(function(d) { 
                    return d.date; 
                })
            );
            y.domain([0, d3.max(dataSet, function(d) { 
                return d.count; 
            })]);

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
            ;

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("Count");

            svg.selectAll(".bar")
                .data(dataSet)
                .enter()
                .append("rect")
                .attr("class", "bar")
                .attr("x", function(d) { return x(d.date); })
                .attr("width", x.rangeBand())
                .attr("y", function(d) { return y(d.count); })
                .attr("height", function(d) { return height - y(d.count); })
            ;

        }
        //on change of any filters, update bar chart
        function updateData() {
            url = baseUrl+"api/student/";
            params = [];
            var isParams = false;
            if($('#ages').val()){
                age_range = $('#ages').val();
                age_range_arr = age_range.split('-');
                params.push("age__gte="+age_range_arr[0]+"&age__lte="+age_range_arr[1]);
                isParams = true;
            }
            if($('#scores').val()){
                score_range = $('#scores').val();
                score_range_arr = score_range.split('-');
                params.push("score__gte="+score_range_arr[0]+"&score__lte="+score_range_arr[1]);
                isParams = true;
            }
            if($('#genders').val()){
                params.push("gender="+$('#genders').val());
                isParams = true;
            }
            if($('#boards').val()){
                params.push("board="+$('#boards').val());
                isParams = true;
            }
            if($('#branches').val()){
                params.push("branch="+$('#branches').val());
                isParams = true;
            }
            if($('#cities').val()){
                params.push("city="+$('#cities').val());
                isParams = true;
            }
            if($('#states').val()){
                params.push("state="+$('#states').val());
                isParams = true;
            }
            if($('#streams').val()){
                params.push("stream="+$('#streams').val());
                isParams = true;
            }
            if(isParams){
                url += '?'+params.join('&');
            }
            $.ajax({
                url: url,
                dataType: "jsonp",
                processData:  false,
                contentType: "application/json",
                crossDomain: true,
                success:function(data){
                    temp = {};
                    dataSet = [];
                    ages = [];
                    $.each(data.objects,function(key,value){
                        if(temp[value.date])
                        temp[value.date] =  temp[value.date] + 1;
                    else
                        temp[value.date] =  1;
                    });
                $.each(temp,function(key,value){
                    dataSet.push({date:key,count:value});
                });
                
                /****************/
                
                x.domain(
                    dataSet.map(function(d) { 
                        return d.date; 
                    })
                );
                y.domain([0, d3.max(dataSet, function(d) { return d.count; })]);
                // d3.select("svg").remove();
                var svg = d3.select("#line-graph")
                            .append("svg")
                            .attr("width", width + margin.left + margin.right)
                            .attr("height", height + margin.top + margin.bottom)
                            .append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


                svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis)
                ;

                svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis)
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", 6)
                    .attr("dy", ".71em")
                    .style("text-anchor", "end")
                    .text("Count")
                ;

                svg.selectAll(".bar")
                    .data(dataSet)
                    .enter()
                    .append("rect")
                    .attr("class", "bar")
                    .attr("x", function(d) { return x(d.date); })
                    .attr("width", x.rangeBand())
                    .attr("y", function(d) { return y(d.count); })
                    .attr("height", function(d) { return height - y(d.count); })
                ;


            /*******************************/
                svgs = $('svg');
                console.log("checking svgs length");
                console.log(svgs);
                svgs[0].remove();
                

            }
        });
    }
    $('.select-filters').on('change',function(){
        //update bar chart
        updateData();
        //reset all pie charts
        $('.pie-chart').html('');
        //update pie charts with new data
        updatePieCharts();
    });
    function type(d) {
        d.count = +d.count;
        return d;
    }

    /******Pie chart code starts******/

    var w =300;
    var h = 300;
    var r = h/2;
    var color = d3.scale.category20c();


    dataSetState = [];
    dataSetBoard = [];
    dataSetStream = [];
    dataSetBranch = [];
    dataSetGender = [];
            
    // ajax call to prepare pie charts data
    url = baseUrl+"api/student/";
        $.ajax({
            url: url,
            dataType: "jsonp",
            processData:  false,
            contentType: "application/json",
            crossDomain: true,
            success:function(data){
                prepareDataSetPie(data);
                branch_headings = $('.branch-heading');
                $('.branch-count').each(function(key,value){
                    $(this).find('h2').html(dataSetBranch[key].count);
                    branch_heading = branch_headings[key];
                    $(branch_heading).find('h5').html(dataSetBranch[key].state);
                });
            }
        });
    //called when filters are selected, to update pie chart data 
    function updatePieCharts(){
        url = baseUrl+"api/student/";
        dataSetState = [];
        dataSetBoard = [];
        dataSetStream = [];
        dataSetBranch = [];
        dataSetGender = [];
    
        params = [];
        var isParams = false;
        if($('#ages').val()){
                age_range = $('#ages').val();
                age_range_arr = age_range.split('-');
                params.push("age__gte="+age_range_arr[0]+"&age__lte="+age_range_arr[1]);
                isParams = true;
        }
            
        if($('#scores').val()){
                score_range = $('#scores').val();
                score_range_arr = score_range.split('-');
                params.push("score__gte="+score_range_arr[0]+"&score__lte="+score_range_arr[1]);
                isParams = true;
        }
        if($('#genders').val()){
            params.push("gender="+$('#genders').val());
            isParams = true;
        }
        if($('#boards').val()){
            params.push("board="+$('#boards').val());
            isParams = true;
        }
        if($('#branches').val()){
            params.push("branch="+$('#branches').val());
            isParams = true;
        }
        if($('#cities').val()){
            params.push("city="+$('#cities').val());
            isParams = true;
        }
        if($('#states').val()){
            params.push("state="+$('#states').val());
            isParams = true;
        }
        if($('#streams').val()){
            params.push("stream="+$('#streams').val());
            isParams = true;
        }
        if(isParams){
            url += '?'+params.join('&');
        }
        $.ajax({
            url: url,
            dataType: "jsonp",
            processData:  false,
            contentType: "application/json",
            crossDomain: true,
            success:function(data){
                prepareDataSetPie(data);
                
            }
        });
        
    }
    function prepareDataSetPie(data){
            tempState = {};
            tempBoard = {};
            tempStream = {};
            tempBranch = {};
            tempGender = {};
            $.each(data.objects,function(key,value){
                // prepareFilters(value);    
                if(tempState[value.state])
                    tempState[value.state] =  tempState[value.state] + 1;
                else
                    tempState[value.state] =  1;
                if(tempBoard[value.board])
                    tempBoard[value.board] =  tempBoard[value.board] + 1;
                else
                    tempBoard[value.board] =  1;
                if(tempStream[value.stream])
                    tempStream[value.stream] =  tempStream[value.stream] + 1;
                else
                    tempStream[value.stream] =  1;
                if(tempBranch[value.branch])
                    tempBranch[value.branch] =  tempBranch[value.branch] + 1;
                else
                    tempBranch[value.branch] =  1;
                if(tempGender[value.gender])
                    tempGender[value.gender] =  tempGender[value.gender] + 1;
                else
                    tempGender[value.gender] =  1;
            });
            $.each(tempState,function(key,value){
                dataSetState.push({state:key,count:value});
            });
            $.each(tempBoard,function(key,value){
                dataSetBoard.push({state:key,count:value});
            });
            $.each(tempStream,function(key,value){
                dataSetStream.push({state:key,count:value});
            });
            $.each(tempBranch,function(key,value){
                dataSetBranch.push({state:key,count:value});
            });
            $.each(tempGender,function(key,value){
                dataSetGender.push({state:key,count:value});
            });
            createPieChart('#chart-state',dataSetState);
            createPieChart('#chart-board',dataSetBoard);
            createPieChart('#chart-stream',dataSetStream);
            createPieChart('#chart-branch',dataSetBranch);
            createPieChart('#chart-gender',dataSetGender);
        }
    //create pie charts from data prepared
    function createPieChart(id,dataSet){
        var vis = d3.select(id).append("svg:svg").data([dataSet]).attr("width", w).attr("height", h).append("svg:g").attr("transform", "translate(" + r + "," + r + ")");
        var pie = d3.layout.pie().value(function(d){return d.count;});

        var arc = d3.svg.arc().outerRadius(r);

        var arcs = vis.selectAll("g.slice").data(pie).enter().append("svg:g").attr("class", "slice");
        arcs.append("svg:path")
            .attr("fill", function(d, i){
                return color(i);
            })
            .attr("d", function (d) {
                return arc(d);
            });

        arcs.append("svg:text").attr("transform", function(d){
                d.innerRadius = 0;
                d.outerRadius = r;
                return "translate(" + arc.centroid(d) + ")";})
                        .attr("text-anchor", "middle")
                        .text( function(d, i) {
                            return dataSet[i].state+" : "+dataSet[i].count;}
                        );    
        }   



});